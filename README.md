What's here?
============


* wallet.stl

A minimalist 3-card wallet I designed, it's meant to look like an hourglass because "Time is Money"

* frame.stl

A hanging postcard display unit, allows viewing both sides of the postcard, and can be interlocked with other copies of itself for displaying multiple postcards. 

* frame2.stl

A second hanging postcard display unit, this one with the intention of designing a more pleasing, flat piece, while still interlocking.

* frame3.stl

A different idea for postcards, this one wall-mounted with an outstretched arm that holds postcards perpendicularly from the wall. Designed to link together like a chain hanging down from the top one, which would be screwed into the wall.

* lightclip.stl

Just a random little adapter clip for attaching a certain brand of LED lights to a different brand of adjustable shelving units.
